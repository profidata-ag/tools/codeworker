#include <string>
#include <iostream>

#include "CW4dl.h"

namespace xentis {
namespace detail {

std::size_t getHash(const std::string& s) {
  std::size_t result = 0;
  for (char c : s) {
    result = 5 * result + c;
  }
  return result;
}
}

CW4DL_EXPORT_SYMBOL const char* getResultCodeId(CW4dl::Interpreter* interpreter,
                                                CW4dl::Parameter facility,
                                                CW4dl::Parameter severity,
                                                CW4dl::Parameter code) {
  long severityId = 0;
  const std::string severityName(static_cast<const char*>(severity));
  if (severityName == "Warning") {
    severityId = 1;
  } else if (severityName == "Success") {
    severityId = 2;
  } else if (severityName == "Error") {
    severityId = 3;
  } else if (severityName == "Information") {
    severityId = 4;
  } else if (severityName == "Fatal") {
    severityId = 5;
  } else if (severityName == "Debug") {
    severityId = 6;
  } else if (severityName == "Trace") {
    severityId = 7;
  }

  const long input1 = 0x20000000l * severityId;
  const long input2 =
      0x00100000l * std::stol(static_cast<const char*>(facility));
  int result = input1 + input2 +
               (detail::getHash(static_cast<const char*>(code)) % 0x000FFFFF);

  return interpreter->copyLocalInt(result);
}

CW4DL_EXPORT_SYMBOL void XENTIS_Init(CW4dl::Interpreter* interpreter) {
  interpreter->createCommand("getResultCodeId", CW4dl::VALUE_PARAMETER,
                             CW4dl::VALUE_PARAMETER, CW4dl::VALUE_PARAMETER);
}

} /* namespace xentis */
